FROM python:3.6-buster


RUN mkdir /code
WORKDIR /code



COPY requirements.txt /code/

RUN apt-get update

RUN apt-get install -y build-essential python3-dev python2.7-dev \
    libldap2-dev libsasl2-dev ldap-utils tox \
    lcov valgrind

RUN pip install -r requirements.txt

COPY . /code/

# Respecto a PYTHONUNBUFFERED https://stackoverflow.com/questions/59812009/what-is-the-use-of-pythonunbuffered-in-docker-file
# ensures that the python output is sent straight to terminal 
ENV PYTHONUNBUFFERED=1

