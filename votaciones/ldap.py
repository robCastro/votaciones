from django_auth_ldap.backend import LDAPBackend


class LDAPBackendRRHH(LDAPBackend):
    """ Conexion de LDAP para buscar usuarios de area RRHH """
    settings_prefix = "AUTH_LDAP_RRHH_"


