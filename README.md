# Instalación

Versión de **python 3.8** (Dejado por Jaime)

Versión de **python 3.6.8** (Estado del servidor de produccion al 23/09/2021)

## Instalar dependencias
~~~
pip install -r requirements.txt
~~~

## Configurar BD
Configurar la BD en **votaciones/settings.py**, luego ejecutar:

~~~
python manage.py makemigrations
python manage.py migrate
~~~

## Instalar static files
~~~
python manage.py collectstatic
~~~

## Ejecutar localmente
~~~
python manage.py runserver
~~~

# Known Issues

## Error con Python LDAP

Al hacer pip install -r requirements.txt es probable que salte error relacionado a python-ldap

        ERROR: Command errored out with exit status 1:
        command: /home/roberto/anaconda3/envs/votaciones/bin/python -u -c 'import io, os, sys, setuptools, tokenize; sys.argv[0] = '"'"'/tmp/pip-install-p6hjx593/python-ldap_b975dbaab48546a3abd2b871bf7ad9a9/setup.py'"'"'; __file__='"'"'/tmp/pip-install-p6hjx593/python-ldap_b975dbaab48546a3abd2b871bf7ad9a9/setup.py'"'"';f = getattr(tokenize, '"'"'open'"'"', open)(__file__) if os.path.exists(__file__) else io.StringIO('"'"'from setuptools import setup; setup()'"'"');code = f.read().replace('"'"'\r\n'"'"', '"'"'\n'"'"');f.close();exec(compile(code, __file__, '"'"'exec'"'"'))' install --record /tmp/pip-record-hsvfrh0g/install-record.txt --single-version-externally-managed --compile --install-headers /home/roberto/anaconda3/envs/votaciones/include/python3.6m/python-ldap

Solventar siguiendo instrucciones: https://stackoverflow.com/questions/4768446/i-cant-install-python-ldap