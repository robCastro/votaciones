from import_export import resources
from import_export.admin import ImportExportModelAdmin

from django.contrib import admin
from .models import DatosEleccion, Terna, Candidato, Voto, Empleado, EmpleadoAd, VotoEmitido

admin.site.site_header = 'Sistema de Votaciones (Panel Administrativo)'


class EmpleadoResource(resources.ModelResource):
    """ Resource para hacer import/export de un excel con empleados """
    class Meta:
        model = Empleado


class EmpleadoAdResource(resources.ModelResource):
    """ Resource para hacer import/export de un excel con empleados o cuentas en el AD"""
    class Meta:
        model = EmpleadoAd


class EmpleadoAdmin(ImportExportModelAdmin):
    """ Pantalla de admin para empleados """
    resource_class = EmpleadoResource
    readonly_fields = ('comprobante',)
    list_display = ('dui', 'usuario', 'nombre', 'cargo', 'lugar', 'unidad', 'padron_presencial', 'comprobante',)
    search_fields = ('dui', 'nombre', 'usuario')
    list_filter = ('padron_presencial', 'cargo', 'lugar',)


class EmpleadoAdAdmin(ImportExportModelAdmin):
    """ Pantalla de admin para empleados """
    resource_class = EmpleadoAdResource
    readonly_fields = ('comprobante',)
    list_display = ('usuario', 'nombre', 'unidad', 'lugar', 'comprobante',)
    search_fields = ('usuario', 'nombre',)
    list_filter = ('unidad', 'lugar',)


class CandidatoStackedInline(admin.StackedInline):
    """ Stacked inline de listado de candidatos por ternas """
    # readonly_fields = ('my_header',)
    model = Candidato
    extra = 0


class TernaAdmin(admin.ModelAdmin):
    """ Configuracion de ternas en el panel de administracion """
    model = Terna
    inlines = (CandidatoStackedInline,)


class DatosEleccionAdmin(admin.ModelAdmin):
    """ Configuracion de datos de eleccion en el panel de administracion """
    model = DatosEleccion
    list_display = ('nombre', 'fecha_inicio', 'fecha_fin', 'estado',)


class VotoEmitidoAdmin(admin.ModelAdmin):
    """
    Configuracion de admin para los votos emitidos
    """
    model = VotoEmitido
    search_fields = ['votante']
    readonly_fiedls = ('referencia',)
    list_display = ('referencia', 'terna', 'votante', 'tipo', 'fecha_voto')
    list_filter = ('terna', 'tipo')
# Register your models here.


admin.site.register(Empleado, EmpleadoAdmin)
admin.site.register(EmpleadoAd, EmpleadoAdAdmin)
admin.site.register(DatosEleccion, DatosEleccionAdmin)
admin.site.register(Terna, TernaAdmin)

admin.site.register(Voto)
admin.site.register(VotoEmitido, VotoEmitidoAdmin)
