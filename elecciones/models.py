from django.db import models
from django.urls import reverse_lazy
from ckeditor.fields import RichTextField

from django.utils.html import mark_safe


class DatosEleccion(models.Model):
    """ Datos generales de la eleccion a ser mostrados en el portal de votacion """
    estado_choice = (
        ('0', 'Votación No Iniciada'),
        ('1', 'Votación En Proceso'),
        ('2', 'Votación Cerrada'),
    )
    titulo = models.CharField(max_length=250, blank=False)
    nombre = models.CharField(max_length=250, blank=False)
    estado = models.CharField(max_length=2, choices=estado_choice, blank=False)
    fecha_inicio = models.DateTimeField(auto_now=False, auto_now_add=False)
    fecha_fin = models.DateTimeField(auto_now=False, auto_now_add=False)
    documento = models.FileField(upload_to="media/base_legal")
    reglamento = models.FileField(upload_to="media/base_legal")
    terminos = RichTextField(verbose_name="Términos y Condiciones")

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name = "Datos Elección"
        verbose_name_plural = "Datos Elecciones"


class Terna(models.Model):
    """ Terna de candidatos """
    nombre = models.CharField(max_length=250, blank=False)

    def __str__(self):
        return self.nombre

    def candidatos(self):
        c = Candidato.objects.filter(terna=self)
        return c

    class Meta:
        verbose_name = "Ternas y Candidatos"
        verbose_name_plural = "Ternas y Candidatos"


class Candidato(models.Model):
    """ Listado de candidatos por terna """
    terna = models.ForeignKey(Terna, on_delete=models.CASCADE)
    foto = models.ImageField()
    nombre = models.CharField(max_length=150, blank=False)
    profesion = models.CharField(max_length=200, blank=True)
    cargo = models.CharField(max_length=200, blank=True)
    linea_trabajo = models.CharField(max_length=250, blank=False)
    info = RichTextField(verbose_name="Reseña")

    def __str__(self):
        return self.nombre

    # def my_header(self):
    #     from django.utils.html import mark_safe
    #     return mark_safe('<img src="{url}" width="{width}" height={height} />'.format(
    #         url=self.foto.url,
    #         width=self.foto.width,
    #         height=self.foto.height
    #     )
    # )


class Empleado(models.Model):
    nombre = models.CharField(max_length=150, blank=False)
    dui = models.CharField(max_length=15, blank=False)
    usuario = models.CharField(max_length=100, blank=True, null=True)
    cargo = models.CharField(max_length=250, blank=False)
    lugar = models.CharField(max_length=250, blank=False)
    unidad = models.CharField(max_length=250, blank=False)
    padron_presencial = models.BooleanField(default=False)

    def __str__(self):
        return self.dui

    def comprobante(self):
        # devuelve el link para generar el comprobante si hay al menos un voto
        # votos = Voto.objects.filter(votante=self.dui)
        votos = VotoEmitido.objects.filter(votante=self.dui)
        if votos.exists():
            return mark_safe(
                "<a href='%s' target='_blank'>Imprimir</a>" % (reverse_lazy('comprobante-tercero', kwargs={'pk': self.dui},))
            )
        else:
            return mark_safe("")


class EmpleadoAd(models.Model):
    """ Para empleados o cuentas registradas en el AD """
    nombre = models.CharField(max_length=200, blank=False)
    usuario = models.CharField(max_length=100, blank=False)
    unidad = models.CharField(max_length=250, blank=False)
    lugar = models.CharField(max_length=250, blank=False)

    def __str__(self):
        return self.usuario

    def comprobante(self):
        # devuelve el link para generar el comprobante si hay al menos un voto
        votos = Voto.objects.filter(votante=self.usuario)
        if votos.exists():
            return mark_safe(
                "<a href='%s' target='_blank'>Imprimir</a>" % (reverse_lazy('comprobante-tercero', kwargs={'pk': self.usuario},))
            )
        else:
            return mark_safe("")


class Voto(models.Model):
    """
    Registra el voto a un candidato, sin registrar quien es el votante (voto secreto)
    """
    candidato = models.ForeignKey(Candidato, on_delete=models.CASCADE)
    votante = models.CharField(max_length=60, blank=True, null=True)  # Puede ser un usuario de AD o un DUI
    fecha_voto = models.DateTimeField(auto_now_add=True)


class VotoEmitido(models.Model):
    """
    Registra si una persona ha emitido su voto, pero no se registra por quien se vota (voto secreto)
    """
    tipos = (
        ('LINEA', 'En Línea'),
        ('PRESENCIAL', 'Presencial')
    )
    # candidato = models.ForeignKey(Candidato, on_delete=models.CASCADE, blank=True, null=True)
    terna = models.ForeignKey(Terna, on_delete=models.CASCADE)
    votante = models.CharField(max_length=60, blank=False)  # Puede ser un usuario de AD o un DUI
    tipo = models.CharField(max_length=100, blank=False, choices=tipos)
    fecha_voto = models.DateTimeField(auto_now_add=True)

    def referencia(self):
        return str(self.id).rjust(5, '0')
