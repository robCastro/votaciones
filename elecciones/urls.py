from django.urls import path
from .views import Inicio, Resultados, Login, TernasView, Candidatos, DetalleCandidatoFree
from .views import DetalleCandidato, CrearVoto, Agradecimiento, NoIniciado, CandidatosFree
from .views import print_pdf, print_pdf_administracion, print_pdf_serial

from .views_presencial import print_pdf as print_pdf_presencial
from .views_presencial import presencial_logout, presencial_impresion
from .views_presencial import LoginPresencial, TernasPresencialView
from .views_presencial import CandidatosPresencial, DetalleCandidatoPresencial
from .views_presencial import CrearVotoPresencial, AgradecimientoPresencial
from .views_presencial import ResultadosPresencial

urlpatterns = [
    path('', Inicio.as_view(), name='inicio'),
    path('resultados/<int:pk>', Resultados.as_view(), name='resultados'),
    path('login', Login.as_view(), name='login'),
    path('ternas', TernasView.as_view(), name='ternas'),
    path('candidatos/<int:terna_id>', Candidatos.as_view(), name='candidatos'),
    path('candidatos_ternas/<int:terna_id>', CandidatosFree.as_view(), name='candidatos-free'),
    path('candidato/<int:terna_id>/<int:pk>', DetalleCandidato.as_view(), name='detalle_candidato'),
    path('candidato_info/<int:terna_id>/<int:pk>', DetalleCandidatoFree.as_view(), name='detalle_candidato_free'),
    path('votar/<int:terna_id>/<int:pk>', CrearVoto.as_view(), name='votar'),
    path('agredecimiento', Agradecimiento.as_view(), name='agradecimiento'),
    path('comprobante', print_pdf, name='comprobante'),
    path('comprobante_tercero/<str:pk>', print_pdf_administracion, name='comprobante-tercero'),
    path('comprobante_serial', print_pdf_serial, name='comprobante-serial'),
    path('noiniciado', NoIniciado.as_view(), name='noiniciado'),
    # presencial
    path('presencial', LoginPresencial.as_view(), name='login-presencial'),
    path('presencial/ternas', TernasPresencialView.as_view(), name='ternas-presencial'),
    path('presencial/candidatos/<int:terna_id>', CandidatosPresencial.as_view(), name='candidatos-presencial'),
    path('presencial/candidato/<int:terna_id>/<int:pk>', DetalleCandidatoPresencial.as_view(), name='detalle_candidato-presencial'),
    path('presencial/votar/<int:terna_id>/<int:pk>', CrearVotoPresencial.as_view(), name='votar-presencial'),
    path('presencial/agredecimiento', AgradecimientoPresencial.as_view(), name='agradecimiento-presencial'),
    path('presencial/comprobante', print_pdf_presencial, name='comprobante-presencial'),
    path('presencial/logout', presencial_logout, name='logout-presencial'),
    path('presencial/impresion', presencial_impresion, name='impresion-presencial'),
    path('presencial/resultados/<int:pk>', ResultadosPresencial.as_view(), name='resultados-presencial'),
]
