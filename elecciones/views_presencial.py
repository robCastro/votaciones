# from django.shortcuts import render
from django.contrib.auth import authenticate, login
from django.urls import reverse_lazy, reverse
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib import messages
from django.views import View
from django.views.generic.list import ListView
from django.views.generic.edit import FormView
from django.views.generic.detail import DetailView

from django.shortcuts import get_object_or_404, redirect, Http404, HttpResponse

from .models import Terna, Candidato, DatosEleccion, Voto, Empleado, VotoEmitido
from .notificacion import EnvioCorreos
from .forms import LoginPresencialForm

from fpdf import FPDF, HTMLMixin
from django.conf import settings


# La contraseña que se usará para la votacion presencial
SECRET_PASSWORD = "asamblea$2022"


def sortVal(val):
    return val["votos"]


class HtmlPdf(FPDF, HTMLMixin):
    pass


class LoginPresencial(FormView):
    """ Vista de login """
    template_name = "presencial/login.html"
    form_class = LoginPresencialForm
    success_url = reverse_lazy('ternas-presencial')

    def get(self, *args, **kwargs):
        detalle_eleccion = DatosEleccion.objects.all()
        if not detalle_eleccion.exists():
            return redirect('noiniciado')
        else:
            if detalle_eleccion.first().estado == '0':
                return redirect('noiniciado')
            if detalle_eleccion.first().estado == '2':
                return redirect('resultados', pk=Terna.objects.all().first().pk)

        return super(LoginPresencial, self).get(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        datos_eleccion = DatosEleccion.objects.all().first()
        ternas = Terna.objects.all()
        context["ternas"] = ternas
        context["datos_eleccion"] = datos_eleccion
        return context

    def form_valid(self, form):
        """ haciendo login """
        # Validar contra lista de padron por AD

        if form.cleaned_data["password"] != SECRET_PASSWORD:
            return self.form_invalid(form)

        usuario = Empleado.objects.filter(dui=form.cleaned_data["username"], padron_presencial=True)

        if usuario.exists():
            # el usuario existe, hay que hacer login
            self.request.session["usuario"] = {"dui": usuario.first().dui, "nombre": usuario.first().nombre}
            # login(self.request, usuario)
        else:
            # Cuenta de usuario no existe, hay que mostrar error
            return self.form_invalid(form)

        return super().form_valid(form)

    def form_invalid(self, form):
        # Cuenta de usuario no existe, hay que mostrar error
        messages.error(self.request, 'Cuenta o contraseña incorrecta')
        return super().form_invalid(form)


class TernasPresencialView(ListView):
    """ Esto es lo que ve un usuario al inicio del proceso de votacion """
    login_url = reverse_lazy('login-presencial')
    model = Terna
    template_name = "presencial/ternas.html"

    def get(self, *args, **kwargs):

        detalle_eleccion = DatosEleccion.objects.all()
        if not detalle_eleccion.exists():
            return redirect('noiniciado')
        else:
            if detalle_eleccion.first().estado == '0':
                return redirect('noiniciado')
            if detalle_eleccion.first().estado == '2':
                return redirect('resultados', pk=Terna.objects.all().first().pk)

        if "usuario" not in self.request.session:
            return redirect('login-presencial')
        if self.request.session["usuario"] is None:
            return redirect('login-presencial')

        return super(TernasPresencialView, self).get(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        datos_eleccion = DatosEleccion.objects.all().first()

        print(self.request.user)

        # verificando si el usuario ha votado en las ternas
        for i in range(len(context["object_list"])):

            votado = VotoEmitido.objects.filter(
                votante=self.request.session["usuario"]["dui"],
                terna__id=context["object_list"][i].pk
            )

            # votado = Voto.objects.filter(
            #     votante=self.request.session["usuario"]["dui"],
            #     candidato__terna__id=context["object_list"][i].pk
            # )

            if votado.exists():
                context["object_list"][i].fecha_voto = votado.first().fecha_voto
                context["object_list"][i].ref = str(votado.first().id).rjust(5, '0')

        context["usuario"] = self.request.session["usuario"]
        context["datos_eleccion"] = datos_eleccion
        return context


class CandidatosPresencial(ListView):
    """ Candidatos filtrados por terna """
    login_url = reverse_lazy('login-presencial')
    model = Terna
    template_name = "presencial/candidatos.html"

    def get(self, *args, **kwargs):

        detalle_eleccion = DatosEleccion.objects.all()
        if not detalle_eleccion.exists():
            return redirect('noiniciado')
        else:
            if detalle_eleccion.first().estado == '0':
                return redirect('noiniciado')
            if detalle_eleccion.first().estado == '2':
                return redirect('resultados', pk=Terna.objects.all().first().pk)

        # Validnado si el usuario esta en sesion
        if "usuario" not in self.request.session:
            return redirect('login-presencial')
        if self.request.session["usuario"] is None:
            return redirect('login-presencial')

        # Validando si el usuario ha votado antes
        votado = Voto.objects.filter(
            candidato__terna__id=self.kwargs["terna_id"],
            votante=self.request.session["usuario"]["dui"]
        )

        if votado.exists():
            return redirect('resultados', pk=self.kwargs["terna_id"])
        else:
            return super(CandidatosPresencial, self).get(*args, **kwargs)

    def get_queryset(self):
        terna_id = self.kwargs['terna_id']
        candidatos = Candidato.objects.filter(terna__id=terna_id)
        return candidatos

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        terna_id = self.kwargs['terna_id']
        datos_eleccion = DatosEleccion.objects.all().first()
        terna = get_object_or_404(Terna, pk=terna_id)
        context["datos_eleccion"] = datos_eleccion
        context["terna"] = terna
        context["usuario"] = self.request.session["usuario"]
        return context


class DetalleCandidatoPresencial(DetailView):
    """ Detalle de datos del candidato """
    model = Candidato
    login_url = reverse_lazy('login-presencial')
    template_name = "presencial/detalle_candidato.html"

    def get(self, *args, **kwargs):

        detalle_eleccion = DatosEleccion.objects.all()
        if not detalle_eleccion.exists():
            return redirect('noiniciado')
        else:
            if detalle_eleccion.first().estado == '0':
                return redirect('noiniciado')
            if detalle_eleccion.first().estado == '2':
                return redirect('resultados', pk=Terna.objects.all().first().pk)

        # Validando si el usuario esta en sesion
        if "usuario" not in self.request.session:
            return redirect('login-presencial')
        if self.request.session["usuario"] is None:
            return redirect('login-presencial')

        # Validando si el usuario ha votado antes
        votado = Voto.objects.filter(
            candidato__terna__id=self.kwargs["terna_id"],
            votante=self.request.session["usuario"]["dui"]
        )

        if votado.exists():
            return redirect('resultados', pk=self.kwargs["terna_id"])
        else:
            return super(DetalleCandidatoPresencial, self).get(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        terna_id = self.kwargs['terna_id']
        datos_eleccion = DatosEleccion.objects.all().first()
        terna = get_object_or_404(Terna, pk=terna_id)
        context["datos_eleccion"] = datos_eleccion
        context["terna"] = terna
        context["usuario"] = self.request.session["usuario"]
        return context


class CrearVotoPresencial(View):
    """ Aca se le da el voto al candidato """
    login_url = reverse_lazy('login-presencial')

    def post(self, request, **kwargs):

        detalle_eleccion = DatosEleccion.objects.all()
        if not detalle_eleccion.exists():
            return redirect('noiniciado')
        else:
            if detalle_eleccion.first().estado == '0':
                return redirect('noiniciado')
            if detalle_eleccion.first().estado == '2':
                return redirect('resultados', pk=Terna.objects.all().first().pk)

        # Validando si el usuario esta en sesion
        if "usuario" not in self.request.session:
            return redirect('login-presencial')
        if self.request.session["usuario"] is None:
            return redirect('login-presencial')

        # Validamos que el usuario no haya emitido antes su voto para la terna

        votado = VotoEmitido.objects.filter(
            terna__id=self.kwargs["terna_id"],
            votante=self.request.session["usuario"]["dui"]
        )

        # votado = Voto.objects.filter(
        #     candidato__terna__id=self.kwargs["terna_id"],
        #     votante=self.request.session["usuario"]["dui"]
        # )

        if votado.exists():
            # Ya ha votado antes, no se debe volver a agregar otro voto a esa terna
            # redireccionar a los resultados de la primera terna y poner mensaje de error
            pterna = Terna.objects.all()
            return redirect('resultados', pk=pterna.first().pk)
            messages.error(request, 'Lo lamentamos, pero tenemos registro que has votaado anteriormente para la terna')
            return redirect('login-presencial')

        candidato = get_object_or_404(Candidato, pk=self.kwargs["pk"])
        usuario = self.request.session["usuario"]["dui"]
        usuario_voto = self.request.session["usuario"]["dui"] if not settings.VOTO_SECRETO else None


        # Creando el voto contable y la comprobacion del voto

        terna = Terna.objects.get(id=self.kwargs["terna_id"])

        Voto.objects.create(
            candidato=candidato,
            votante=usuario_voto
        )

        VotoEmitido.objects.create(terna=terna, votante=usuario, tipo='PRESENCIAL')

        return redirect('agradecimiento-presencial')


class AgradecimientoPresencial(ListView):
    """ Esto es lo que ve un usuario al inicio del proceso de votacion """
    login_url = reverse_lazy('login-presencial')
    model = Terna
    template_name = "presencial/agradecimiento.html"

    def get(self, *args, **kwargs):
        detalle_eleccion = DatosEleccion.objects.all()
        if not detalle_eleccion.exists():
            return redirect('noiniciado')
        else:
            if detalle_eleccion.first().estado == '0':
                return redirect('noiniciado')
            if detalle_eleccion.first().estado == '2':
                return redirect('resultados', pk=Terna.objects.all().first().pk)
        # Validando si el usuario esta en sesion
        if "usuario" not in self.request.session:
            return redirect('login-presencial')
        if self.request.session["usuario"] is None:
            return redirect('login-presencial')

        return super(AgradecimientoPresencial, self).get(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        datos_eleccion = DatosEleccion.objects.all().first()

        # verificando si el usuario ha votado en las ternas
        for i in range(len(context["object_list"])):

            votado = VotoEmitido.objects.filter(
                votante=self.request.session["usuario"]["dui"],
                terna__id=context["object_list"][i].pk
            )

            # votado = Voto.objects.filter(
            #     votante=self.request.session["usuario"]["dui"],
            #     candidato__terna__id=context["object_list"][i].pk
            # )

            if votado.exists():
                context["object_list"][i].fecha_voto = votado.first().fecha_voto
                context["object_list"][i].ref = str(votado.first().id).rjust(5, '0')

        context["datos_eleccion"] = datos_eleccion
        context["usuario"] = self.request.session["usuario"]
        return context


# Generacion de comprobante
def print_pdf(request):

    if request.user.is_authenticated:

        from django.template.loader import render_to_string

        # votos = Voto.objects.filter(votante=request.session["usuario"]["dui"])
        votos = Voto.objects.filter(votante=request.session["usuario"]["dui"])
        eleccion = DatosEleccion.objects.all()

        if not votos.exists():
            raise Http404()

        for i in range(len(votos)):
            votos[i].ref = str(votos[i].id).rjust(5, '0')

        pdf = HtmlPdf()
        pdf.add_page()
        pdf.write_html(render_to_string(
            'presencial/comprobante.html',
            {"votos": votos, "eleccion": eleccion.first(), "usuario": request.session["usuario"]["nombre"]}
        ))

        response = HttpResponse(pdf.output(dest='S').encode('latin-1'))
        response['Content-Type'] = 'application/pdf'

        return response

    else:
        raise Http404()


# vista impresion presencial:
def presencial_impresion(request):

    if request.method == 'POST':
        password = request.POST.get('password')
        if password == SECRET_PASSWORD:
            return redirect('comprobante-presencial')
        else:
            messages.error(request, 'Usuario o contraseña incorrecta')
            return redirect('agradecimiento-presencial')
    else:
        messages.error(request, 'Metodo incorrecto')
        return redirect('agradecimiento-presencial')


# logout
def presencial_logout(request):

    if request.method == 'POST':
        password = request.POST.get('password')
        if password == SECRET_PASSWORD:
            if "usuario" in request.session:
                del request.session["usuario"]
            return redirect('login-presencial')
        else:
            messages.error(request, 'Usuario o contraseña incorrecta')
            return redirect('agradecimiento-presencial')
    else:
        messages.error(request, 'Metodo incorrecto')
        return redirect('agradecimiento-presencial')


class ResultadosPresencial(DetailView):
    """ Aca vamos a filtrar por id de terna y ademas contar votos """
    model = Terna
    template_name = "presencial/resultados_presencial.html"

    def get(self, *args, **kwargs):

        detalle_eleccion = DatosEleccion.objects.all()
        if not detalle_eleccion.exists():
            return redirect('noiniciado')
        else:
            if detalle_eleccion.first().estado == '0':
                return redirect('noiniciado')

        return super(ResultadosPresencial, self).get(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        datos_eleccion = DatosEleccion.objects.all().first()
        context["datos_eleccion"] = datos_eleccion
        ternas = Terna.objects.all()
        context["ternas"] = ternas

        # Candidatos
        candidatos = context["object"].candidatos()

        # Obteniendo el array de nombres de candidatos
        candidatos_names = ["%s" % candidato.nombre for candidato in candidatos]

        # Contando votos
        votos = list()
        for candidato in candidatos:
            votos.append(len(Voto.objects.filter(candidato=candidato)))

        # Para armar la tabla
        tabla = list()
        for i in range(len(candidatos)):
            item = {
                "nombre": candidatos[i].nombre,
                "indice": i,
                "foto": candidatos[i].foto,
                "votos": votos[i]
            }
            tabla.append(item)

        tabla.sort(key=sortVal, reverse=True)

        resultados = {
            "nombres": candidatos_names,
            "votos": [total for total in votos],
            "tabla": tabla
        }

        context["resultados"] = resultados

        return context
