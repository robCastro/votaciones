# from django.shortcuts import render
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.urls import reverse_lazy  # , reverse
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib import messages
from django.views import View
from django.views.generic import TemplateView
from django.views.generic.list import ListView
from django.views.generic.edit import FormView
from django.views.generic.detail import DetailView

from django.shortcuts import get_object_or_404, redirect, Http404, HttpResponse

from .models import Terna, Candidato, DatosEleccion, Voto, Empleado, EmpleadoAd, VotoEmitido
from .notificacion import EnvioCorreos
from .forms import LoginForm

from fpdf import FPDF, HTMLMixin
from django.conf import settings


def sortVal(val):
    return val["votos"]


class HtmlPdf(FPDF, HTMLMixin):
    pass


class NoIniciado(ListView):
    """ Si la eleccion aun no ha empezado """
    template_name = "noiniciado.html"
    model = Terna

    def get(self, *args, **kwargs):
        detalle_eleccion = DatosEleccion.objects.all()
        if detalle_eleccion.first().estado != '0':
            return redirect('inicio')
        return super(NoIniciado, self).get(*args, **kwargs)


class Inicio(ListView):
    """ Pantalla Inicial, unicamente se muestra listado de ternas """
    model = Terna
    template_name = "inicio.html"

    def get(self, *args, **kwargs):
        detalle_eleccion = DatosEleccion.objects.all()
        if not detalle_eleccion.exists():
            return redirect('noiniciado')
        else:
            if detalle_eleccion.first().estado == '0':
                return redirect('noiniciado')

        # Haciendo logout si accede aca para forzar volver a poner credenciales
        if self.request.user.is_authenticated:
            logout(self.request)
        return super(Inicio, self).get(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        datos_eleccion = DatosEleccion.objects.all().first()
        context["datos_eleccion"] = datos_eleccion
        return context


class Resultados(DetailView):
    """ Aca vamos a filtrar por id de terna y ademas contar votos """
    model = Terna
    template_name = "resultados.html"

    def get(self, *args, **kwargs):

        detalle_eleccion = DatosEleccion.objects.all()
        if not detalle_eleccion.exists():
            return redirect('noiniciado')
        else:
            if detalle_eleccion.first().estado == '0':
                return redirect('noiniciado')

        # Haciendo logout si accede aca para forzar volver a poner credenciales
        if self.request.user.is_authenticated:
            logout(self.request)
        return super(Resultados, self).get(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        datos_eleccion = DatosEleccion.objects.all().first()
        context["datos_eleccion"] = datos_eleccion
        ternas = Terna.objects.all()
        context["ternas"] = ternas

        # Candidatos
        candidatos = context["object"].candidatos()

        # Obteniendo el array de nombres de candidatos
        candidatos_names = ["%s" % candidato.nombre for candidato in candidatos]

        # Contando votos
        votos = list()
        for candidato in candidatos:
            votos.append(len(Voto.objects.filter(candidato=candidato)))

        # Para armar la tabla
        tabla = list()
        for i in range(len(candidatos)):
            item = {
                "nombre": candidatos[i].nombre,
                "indice": i,
                "foto": candidatos[i].foto,
                "votos": votos[i]
            }
            tabla.append(item)

        tabla.sort(key=sortVal, reverse=True)

        resultados = {
            "nombres": candidatos_names,
            "votos": [total for total in votos],
            "tabla": tabla
        }

        context["resultados"] = resultados

        return context


class Login(FormView):
    """ Vista de login """
    template_name = "login.html"
    form_class = LoginForm
    success_url = reverse_lazy('ternas')

    def get(self, *args, **kwargs):
        detalle_eleccion = DatosEleccion.objects.all()
        if not detalle_eleccion.exists():
            return redirect('noiniciado')
        else:
            if detalle_eleccion.first().estado == '0':
                return redirect('noiniciado')
            if detalle_eleccion.first().estado == '2':
                return redirect('resultados', pk=Terna.objects.all().first().pk)
        # Haciendo logout si accede aca para forzar volver a poner credenciales
        if self.request.user.is_authenticated:
            logout(self.request)
        return super(Login, self).get(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        datos_eleccion = DatosEleccion.objects.all().first()
        ternas = Terna.objects.all()
        context["ternas"] = ternas
        context["datos_eleccion"] = datos_eleccion
        return context

    def form_valid(self, form):
        """ haciendo login """

        usuario = authenticate(
            username=form.cleaned_data['username'],
            password=form.cleaned_data['password']
        )

        if usuario is not None:
            # el usuario existe, hay que hacer login
            login(self.request, usuario)
        else:
            # Cuenta de usuario no existe, hay que mostrar error
            return self.form_invalid(form)

        return super().form_valid(form)

    def form_invalid(self, form):
        # Cuenta de usuario no existe, hay que mostrar error
        messages.error(self.request, 'Cuenta o contraseña incorrecta')
        return super().form_invalid(form)


class TernasView(LoginRequiredMixin, ListView):
    """ Esto es lo que ve un usuario al inicio del proceso de votacion """
    login_url = reverse_lazy('login')
    model = Terna
    template_name = "ternas.html"

    def get(self, *args, **kwargs):
        try:
            votante = Empleado.objects.get(usuario=self.request.user.username)
        except Empleado.DoesNotExist:
            # Votante no está en padron
            messages.error(self.request, 'No se encuentra en el padrón de votación. Puede comunicarse con RRHH')
            return redirect('login')
        detalle_eleccion = DatosEleccion.objects.all()
        if not detalle_eleccion.exists():
            return redirect('noiniciado')
        else:
            if detalle_eleccion.first().estado == '0':
                return redirect('noiniciado')
            if detalle_eleccion.first().estado == '2':
                return redirect('resultados', pk=Terna.objects.all().first().pk)

        return super(TernasView, self).get(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        datos_eleccion = DatosEleccion.objects.all().first()

        # verificando si el usuario ha votado en las ternas
        for i in range(len(context["object_list"])):
            try:
                votante = Empleado.objects.get(usuario=self.request.user.username)
            except Empleado.DoesNotExist:
                # Votante no está en padron
                votante = None

            votado = VotoEmitido.objects.filter(
                votante=votante.dui,
                terna__id=context["object_list"][i].pk
            )

            # votado = Voto.objects.filter(
            #     votante=self.request.user.username,
            #     candidato__terna__id=context["object_list"][i].pk
            # )

            if votado.exists():
                context["object_list"][i].fecha_voto = votado.first().fecha_voto
                context["object_list"][i].ref = str(votado.first().id).rjust(5, '0')

        context["datos_eleccion"] = datos_eleccion
        return context


class Candidatos(LoginRequiredMixin, ListView):
    """ Candidatos filtrados por terna """
    login_url = reverse_lazy('login')
    model = Terna
    template_name = "candidatos.html"

    def get(self, *args, **kwargs):
        detalle_eleccion = DatosEleccion.objects.all()
        if not detalle_eleccion.exists():
            return redirect('noiniciado')
        else:
            if detalle_eleccion.first().estado == '0':
                return redirect('noiniciado')
            if detalle_eleccion.first().estado == '2':
                return redirect('resultados', pk=Terna.objects.all().first().pk)
        # Validando si el usuario ha votado antes
        votado = Voto.objects.filter(
            candidato__terna__id=self.kwargs["terna_id"],
            votante=self.request.user.username
        )

        if votado.exists():
            return redirect('resultados', pk=self.kwargs["terna_id"])
        else:
            return super(Candidatos, self).get(*args, **kwargs)

    def get_queryset(self):
        terna_id = self.kwargs['terna_id']
        candidatos = Candidato.objects.filter(terna__id=terna_id)
        return candidatos

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        terna_id = self.kwargs['terna_id']
        datos_eleccion = DatosEleccion.objects.all().first()
        terna = get_object_or_404(Terna, pk=terna_id)
        context["datos_eleccion"] = datos_eleccion
        context["terna"] = terna
        return context


class CandidatosFree(ListView):
    """ Candidatos filtrados por terna """
    model = Terna
    template_name = "candidatos_free.html"

    def get_queryset(self):
        terna_id = self.kwargs['terna_id']
        candidatos = Candidato.objects.filter(terna__id=terna_id)
        return candidatos

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        terna_id = self.kwargs['terna_id']
        datos_eleccion = DatosEleccion.objects.all().first()
        terna = get_object_or_404(Terna, pk=terna_id)
        context["datos_eleccion"] = datos_eleccion
        context["terna"] = terna
        return context


class DetalleCandidato(LoginRequiredMixin, DetailView):
    """ Detalle de datos del candidato """
    model = Candidato
    login_url = reverse_lazy('login')
    template_name = "detalle_candidato.html"

    def get(self, *args, **kwargs):
        detalle_eleccion = DatosEleccion.objects.all()
        if not detalle_eleccion.exists():
            return redirect('noiniciado')
        else:
            if detalle_eleccion.first().estado == '0':
                return redirect('noiniciado')
            if detalle_eleccion.first().estado == '2':
                return redirect('resultados', pk=Terna.objects.all().first().pk)
        # Validando si el usuario ha votado antes
        votado = Voto.objects.filter(
            candidato__terna__id=self.kwargs["terna_id"],
            votante=self.request.user.username
        )

        if votado.exists():
            return redirect('resultados', pk=self.kwargs["terna_id"])
        else:
            return super(DetalleCandidato, self).get(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        terna_id = self.kwargs['terna_id']
        datos_eleccion = DatosEleccion.objects.all().first()
        terna = get_object_or_404(Terna, pk=terna_id)
        context["datos_eleccion"] = datos_eleccion
        context["terna"] = terna
        return context


class DetalleCandidatoFree(DetailView):
    """ Detalle de datos del candidato """
    model = Candidato
    template_name = "detalle_candidato_free.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        terna_id = self.kwargs['terna_id']
        datos_eleccion = DatosEleccion.objects.all().first()
        terna = get_object_or_404(Terna, pk=terna_id)
        context["datos_eleccion"] = datos_eleccion
        context["terna"] = terna
        return context


class CrearVoto(LoginRequiredMixin, View):
    """ Aca se le da el voto al candidato """
    login_url = reverse_lazy('login')

    def post(self, request, **kwargs):

        detalle_eleccion = DatosEleccion.objects.all()
        if not detalle_eleccion.exists():
            return redirect('noiniciado')
        else:
            if detalle_eleccion.first().estado == '0':
                return redirect('noiniciado')
            if detalle_eleccion.first().estado == '2':
                return redirect('resultados', pk=Terna.objects.all().first().pk)

        # Validamos que el usuario no haya emitido antes su voto para la terna

        empleado = Empleado.objects.get(usuario=request.user.username)

        votado = VotoEmitido.objects.filter(
            terna__id=self.kwargs["terna_id"],
            votante=empleado.dui,
        )

        # votado = Voto.objects.filter(
        #     candidato__terna__id=self.kwargs["terna_id"],
        #     votante=self.request.user.username
        # )

        if votado.exists():
            # Ya ha votado antes, no se debe volver a agregar otro voto a esa terna
            # redireccionar a los resultados de la primera terna y poner mensaje de error
            pterna = Terna.objects.all()
            return redirect('resultados', pk=pterna.first().pk)
            messages.error(request, 'Lo lamentamos, pero tenemos registro que has votaado anteriormente para la terna')
            return redirect('inicio')

        candidato = get_object_or_404(Candidato, pk=self.kwargs["pk"])

        # usuario = request.user.username
        # se verifica si el voto es o no secreto
        usuario_voto = empleado.dui if not settings.VOTO_SECRETO else None
        usuario = empleado.dui
        # Creando el voto contable a resultados y el voto emitido

        Voto.objects.create(
            candidato=candidato,
            votante=usuario_voto
        )

        VotoEmitido.objects.create(
            terna=candidato.terna,
            votante=usuario,
            tipo='LINEA'
        )

        return redirect('agradecimiento')


class Agradecimiento(LoginRequiredMixin, ListView):
    """ Esto es lo que ve un usuario al inicio del proceso de votacion """
    login_url = reverse_lazy('login')
    model = Terna
    template_name = "agradecimiento.html"

    def get(self, *args, **kwargs):
        detalle_eleccion = DatosEleccion.objects.all()
        if not detalle_eleccion.exists():
            return redirect('noiniciado')
        else:
            if detalle_eleccion.first().estado == '0':
                return redirect('noiniciado')
            if detalle_eleccion.first().estado == '2':
                return redirect('resultados', pk=Terna.objects.all().first().pk)

        return super(Agradecimiento, self).get(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        datos_eleccion = DatosEleccion.objects.all().first()

        # verificando si el usuario ha votado en las ternas
        for i in range(len(context["object_list"])):

            empleado = Empleado.objects.get(usuario=self.request.user.username)

            votado = VotoEmitido.objects.filter(
                votante=empleado.dui,
                terna_id=context["object_list"][i].pk
            )

            # votado = Voto.objects.filter(
            #     votante=self.request.user.username,
            #     candidato__terna__id=context["object_list"][i].pk
            # )

            if votado.exists():
                context["object_list"][i].fecha_voto = votado.first().fecha_voto
                context["object_list"][i].ref = str(votado.first().id).rjust(5, '0')

        context["datos_eleccion"] = datos_eleccion
        return context


# Generacion de comprobante
def print_pdf(request):

    if request.user.is_authenticated:

        from django.template.loader import render_to_string

        # votos = Voto.objects.filter(votante=request.user.username)
        votante = Empleado.objects.get(usuario=request.user.username)
        votos = VotoEmitido.objects.filter(votante=votante.dui)
        eleccion = DatosEleccion.objects.all()

        if not votos.exists():
            raise Http404()

        for i in range(len(votos)):
            votos[i].ref = str(votos[i].id).rjust(5, '0')

        pdf = HtmlPdf()
        pdf.add_page()
        pdf.write_html(render_to_string(
            'comprobante.html',
            {"votos": votos, "eleccion": eleccion.first(), "usuario": request.user.first_name + ' ' + request.user.last_name}
        ))

        response = HttpResponse(pdf.output(dest='S').encode('latin-1'))
        response['Content-Type'] = 'application/pdf'

        return response

    else:
        raise Http404()


# Generacion de comprobante desde panel de administracion
def print_pdf_administracion(request, pk):

    if request.user.is_authenticated:
        # Unicamente usuarios en el grupo imprimir_comprobante podra imprimir
        if User.objects.filter(groups__name="imprimir_comprobante", username=request.user.username).exists():

            identificador = pk
            from django.template.loader import render_to_string

            # votos = Voto.objects.filter(votante=identificador)
            votos = VotoEmitido.objects.filter(votante=identificador)
            eleccion = DatosEleccion.objects.all()

            empleado = Empleado.objects.filter(dui=pk)

            if not empleado.exists():
                empleado = EmpleadoAd.objects.filter(usuario=pk)

            if not empleado.exists():
                raise Http404()

            emp = empleado.first()

            if not votos.exists():
                raise Http404()

            for i in range(len(votos)):
                votos[i].ref = str(votos[i].id).rjust(5, '0')

            pdf = HtmlPdf()
            pdf.add_page()
            pdf.write_html(render_to_string(
                'comprobante.html',
                {"votos": votos, "eleccion": eleccion.first(), "usuario": emp.nombre}
            ))

            response = HttpResponse(pdf.output(dest='S').encode('latin-1'))
            response['Content-Type'] = 'application/pdf'

            return response

        else:
            raise Http404()
    else:
        raise Http404()


# Generacion de comprobantes en serie
def print_pdf_serial(request):

    if request.user.is_authenticated:

        # Unicamente usuarios en el grupo imprimir_comprobante podra imprimir
        if User.objects.filter(groups__name="imprimir_comprobante", username=request.user.username).exists():

            from django.template.loader import render_to_string
            empleados_all = Empleado.objects.all().order_by('pk')
            eleccion = DatosEleccion.objects.all()
            pdf = HtmlPdf()
            pdf.set_auto_page_break(0)

            for empleado in empleados_all:

                identificador = empleado.dui

                # votos = Voto.objects.filter(votante=identificador)
                votos = VotoEmitido.objects.filter(votante=identificador)

                # empleado = Empleado.objects.filter(dui=pk)

                # if not empleado.exists():
                #    empleado = EmpleadoAd.objects.filter(usuario=pk)

                # if not empleado.exists():
                #    raise Http404()

                emp = empleado

                if not votos.exists():
                    # raise Http404()
                    pass
                else:

                    for i in range(len(votos)):
                        votos[i].ref = str(votos[i].id).rjust(5, '0')
                        # pdf = HtmlPdf()
                        # print("agregando pagina")
                        # print(emp.nombre)
                        # print("votos")
                        # print(votos)
                        pdf.add_page()

                        pdf.write_html(render_to_string(
                            'comprobante.html',
                            {"votos": votos, "eleccion": eleccion.first(), "usuario": emp.nombre}
                        ))

                        # pdf.write_html(render_to_string(
                        #     'comprobante.html',
                        #     {"votos": votos, "eleccion": eleccion.first(), "usuario": emp.nombre}
                        # ))

            response = HttpResponse(pdf.output(dest='S').encode('latin-1'))
            response['Content-Type'] = 'application/pdf'

            return response

        else:
            raise Http404()
    else:
        raise Http404()
