from django.core.mail import EmailMultiAlternatives
# from django.core.mail import send_mail
from django.conf import settings

# from django.template.loader import get_template


class EnvioCorreos:

    def confirmacion_voto(self, voto, destinatario):
        """ Enviando un correo para notificar sobre el voto """
        subject = "Confirmacion Voto: %s " % voto.id

        text_content = "Tu voto para %s fue realizado el %s el código de referencia es '%s', este correo es enviado automáticamente, por favor No enviar una respuesta a esta cuenta" % (voto.candidato.terna.nombre, voto.fecha_voto, voto.id)
        html_content = "Tu voto para %s fue realizado el %s el código de referencia es '%s', este correo es enviado automáticamente, por favor No enviar una respuesta a esta cuenta" % (voto.candidato.terna.nombre, voto.fecha_voto, voto.id)

        from_email = settings.EMAIL_HOST_USER
        to_email = [destinatario]
        try:
            print('from_email: %s' % from_email)
            print('to_email:')
            print(destinatario)
            msg = EmailMultiAlternatives(subject, text_content, from_email, to_email)
            msg.attach_alternative(html_content, "text/html")
            msg.send()
        except Exception as e:
            # raise e
            print("error email")
            print(e)
            pass
