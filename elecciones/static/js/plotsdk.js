class ResultadoGraph {
    
    draw(componente, data, layout) {

        let config = {
            'displayModeBar': false, // this is the line that hides the bar.
            'scrollZoom': false,
            'staticPlot': true
        };

        Plotly.newPlot(componente, data, layout, config)

    }

}