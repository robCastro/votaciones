$(document).ready(function(){

    $('#terminos').click(function(e){
        e.preventDefault()
        $('#terminosModal').modal('show')
    })

    $('#cerrar').click(function(){
        $('#terminosModal').modal('hide')
    })

    $('#cerrar2').click(function(){
        $('#terminosModal').modal('hide')
    })

    $('#confirmarVoto').click(function(e){
        e.preventDefault()
        $('#confirmacionVotoModal').modal('show')
    })

    $('#cerrarConfirmarVoto').click(function(e){
        e.preventDefault()
        $('#confirmacionVotoModal').modal('hide')
    })
    $('#cerrarConfirmarVoto2').click(function(e){
        e.preventDefault()
        $('#confirmacionVotoModal').modal('hide')
    })

    $('#mostrarCredenciales').click(function(e){
        e.preventDefault()
        $('#credencialesModal').modal('show')
    })
    $('#cerrarCredenciales').click(function(e){
        e.preventDefault()
        $('#credencialesModal').modal('hide')
    })
    $('#cerrarCredenciales2').click(function(e){
        e.preventDefault()
        $('#credencialesModal').modal('hide')
    })


    $('#mostrarCredencialesImpresion').click(function(e){
        e.preventDefault()
        $('#impresionModal').modal('show')
    })
    $('#cerrarImpresion').click(function(e){
        e.preventDefault()
        $('#impresionModal').modal('hide')
    })
    $('#cerrarImpresion2').click(function(e){
        e.preventDefault()
        $('#impresionModal').modal('hide')
    })

})