from django import forms


class LoginForm(forms.Form):
    """ Formulario para hacer login """
    username = forms.CharField(
        label="Cuenta", max_length=150, required=True,
        widget=forms.TextInput(attrs={
            "class": "form-control",
            "placeholder": "Es la misma con la que ingresas a tu PC"
        })
    )
    password = forms.CharField(
        label="Contraseña", max_length=150,
        widget=forms.PasswordInput(attrs={
            "class": "form-control",
            "placeholder": "Ingresa tu contraseña"
        })
    )

class LoginPresencialForm(forms.Form):
    """ Formulario para hacer login """
    username = forms.CharField(
        label="Cuenta", max_length=150, required=True,
        widget=forms.TextInput(attrs={
            "class": "form-control",
            "placeholder": "00000000-0"
        })
    )
    password = forms.CharField(
        label="Contraseña", max_length=150,
        widget=forms.PasswordInput(attrs={
            "class": "form-control",
            "placeholder": "Ingresa la contraseña"
        })
    )
